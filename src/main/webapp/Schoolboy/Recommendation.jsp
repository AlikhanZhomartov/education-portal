<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/Result.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
</head>

<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>
            <p class="logo__title">
                Job. S
            </p>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletTestsSelect">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Schoolboy/Settings.jsp">���������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>

            </ul>

        </nav>

    </div>
</header>

<%String result = (String) request.getAttribute("result");%>
<section class="Result">

    <div class="container">

        <div class="result__block">

            <h3 class="final">
                ���� ������������
            </h3>

            <p class="result__text">
             <%=result%>
            </p>


        </div>

    </div>

</section>

<section class="university">

    <div class="container">

        <h3 class="uni__list">
            ������ ��������������� �������������
        </h3>
        <c:forEach var="uni" items="${University}">
        <div class="university__block">

            <a class="title__uni" href="${uni.uni_link}">
                ${uni.uni_title}
            </a>
            <ul class="btn_list">
                <li class="btn__item">${uni.uni_type}</li>
                <li class="btn__item">${uni.uni_city}</li>
            </ul>
            <p class="uni__text">
                    ${uni.uni_description}
            </p>
        </div>
        </c:forEach>
    </div>

</section>

</body>

</html>