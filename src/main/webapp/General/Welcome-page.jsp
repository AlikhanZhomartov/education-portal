<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/Welcome-page.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
</head>

<body>


<header class="header" id="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>

            <p class="logo__title">
                Job. S
            </p>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">

                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/General/Authorization.jsp">����</a>
                </li>
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/General/Registration.jsp">�����������</a>
                </li>

            </ul>

        </nav>
    </div>

</header>


<section class="description__jobS">

    <div class="container">

        <div class="description__inner">

            <p class="description__text">Job S.(Job searching) - ��� ���������� ���������, ������������� ����������
                ��� ���������, ������������� � ����������.
                ������� ������ ��������� ������� � ���������������.���� �� ��������, �� ������ ������ ���� ��
                �������� ������ �
                ����� ������� ���������. ��� ��������� ��� ��������� ���������� �� ����� �������� �� ���������������
                �������������.��������������, ������������ ������� �������� � ����� ����������� �������.
            </p>

            <div class="description__links">
                <a class="students__link links" href="#description_students">
                    ��� ���������
                </a>
                <a class="schoolboy__link links" href="#description_schoolboy">
                    ��� ����������
                </a>
                <a class="employee__link links" href="#description_employee">
                    ��� �������������
                </a>
            </div>

        </div>

    </div>
</section>
<section class="description_students" id="description_students">

    <div class="container">


        <div class="description_students__inner general__inner">

            <h3 class="student_title general__title">
                ��������
            </h3>
            <div class="student__text__image general__text__image">
                <p class="description_students__text general__text">
                    ���� ��������� ��������� ���, ��� ��������� ����������� ��������� ����� ������ ������.��
                    �����
                    �� ������ ������� ������ � �������� ��������������� ��� ��������. ��� ���������� ����
                    ���������
                    �������� ����� �����������.
                </p>

                <img class="students__img general__image" src="<%=request.getContextPath()%>/resources/images/student_image.jpg" alt="student">
            </div>
            <a class="sudent_registration general__registration" href="<%=request.getContextPath()%>/General/Registration.jsp">
                �����������
            </a>
        </div>

    </div>

</section>

<section class="description_schoolboy" id="description_schoolboy">

    <div class="container">


        <div class="description_schoolboy__inner general__inner">

            <h3 class="schoolboy_title general__title">
                ���������
            </h3>
            <div class="schoolboy__text__image general__text__image">
                <img class="schoolboy__img general__image" src="<%=request.getContextPath()%>/resources/images/schoolboy_image.jpg" alt="schoolboy">
                <p class="description_schoolboy__text general__text">
                    Job S.- ��� ����������� �� ������ ��� ��������� � �������������, �� ������������ � � ����������.
                    ��������� ���������, ����������������� ������, ��������� ������ � ��������� ������� ����
                    hard&soft skills. ��������� �����, ��� ������� ��� ���� ����� ����������� � ������� ����� �
                    �������������.
                </p>
            </div>
            <a class="schoolboy_registration general__registration" href="<%=request.getContextPath()%>/General/Registration.jsp">
                �����������
            </a>
        </div>

    </div>

</section>



<section class="description_employee" id="description_employee">

    <div class="container">


        <div class="description_employee__inner general__inner">

            <h3 class="employee_title general__title">
                ������������
            </h3>
            <div class="employee__text__image general__text__image">
                <p class="description_employee__text general__text">
                    ��� �� �������� �� ��������, ������� ������ ����� ��������� ��������� �� �������������. � �����
                    ������ ��������, ��� ����� ����������� ������� ��������, ����������� ������� � � ����������
                    �������� ����������� ������������.
                </p>

                <img class="employee__img general__image" src="<%=request.getContextPath()%>/resources/images/employee_image.jpg" alt="employee">
            </div>
            <a class="employee_registration general__registration" href="<%=request.getContextPath()%>/General/Registration.jsp">
                �����������
            </a>
        </div>

    </div>

</section>

<footer class="footer">
    <a class="arrow arrow-top" href="#header"></a>
</footer>

</body>

</html>