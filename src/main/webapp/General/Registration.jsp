<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/Registration.css"/>
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
</head>

<body>


<section class="registration__section">

    <div class="container">

        <div class="registration-block">

            <h1 class="registration__title">�����������</h1>
            <form method="post" action="<%=request.getContextPath()%>/ServletRegistration">
            <input class="registration__input" name="Email" placeholder="Email" autocomplete="off">
            <input class="registration__input" name="Password" placeholder="Password" autocomplete="off">
            <input class="registration__input" name="Name" placeholder="Name" autocomplete="off">
            <input class="registration__input" name="Surname" placeholder="Surname" autocomplete="off">
            <div class="radio__btns">
                <label>
                    <input type="radio" class="radio__schoolboy radio" name="Gender" id="radio__schoolboy" value="schoolboy" autocomplete="off">
                    <span class="radio__span">��������</span>
                </label>
                <label>
                    <input type="radio" class="radio__student radio"  name="Gender" id="radio__student" value="student" autocomplete="off">
                    <span class="radio__span">�������</span>
                </label>
                <label>
                    <input type="radio" class="radio__employee radio" name="Gender" id="radio__employee" value="employee" autocomplete="off">
                    <span class="radio__span">������������</span>
                </label>
            </div>
            <button type="submit" class="registration__submit">�����������</button>
            </form>

            <div class="links">
            <a href="<%=request.getContextPath()%>/General/Authorization.jsp" class="authorization__link">�����������</a>
            <a href="<%=request.getContextPath()%>/General/Welcome-page.jsp" class="authorization__link">�� �������</a>
            </div>

        </div>

    </div>

</section>

</body>

</html>