<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.edu.astanait.Responses.Responses" %>
<%@ page import="java.util.Iterator" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/Responses.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
</head>

<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link" href="#">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>

            <form method="post" action="<%=request.getContextPath()%>/ServletListOfVacancyForEmployee">
                <button class="header__btn">
                    ��������
                </button>
            </form>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Employee/CreateVacancy.jsp">������� ��������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletEmployeeVacancy">
                        <button class="menu__list-link__btn">��� ��������</button>
                    </form>
                </li>
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Employee/Settings.jsp">���������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>

            </ul>

        </nav>


    </div>

</header>

<%ArrayList<Responses> responses = (ArrayList<Responses>) request.getAttribute("responses");%>

<section class="section__responses">

    <div class="container">

        <form method="post" action="<%=request.getContextPath()%>/ServletDeleteResponses">
        <button class="clear__all">
            �������� ���
        </button>
        </form>

        <%for(int i = 0; i < responses.size(); i+=3){%>
        <div class="responses__inner">

            <div class="responses__inner__blocks">

                <h4 class="response__name__surname">
                    <%=responses.get(i).getSurname()%> <%=responses.get(i).getName()%>
                </h4>
                <p class="response__phone">
                    <%=responses.get(i).getNumber()%>
                </p>
                <p class="response__vacancy__title">
                    <%=responses.get(i).getJob_title()%>
                </p>
                <p class="response__company">
                    <%=responses.get(i).getCompany_title()%>
                </p>
                <p class="response__city">
                    <%=responses.get(i).getJober_city()%>
                </p>
                <form method="post" action="<%=request.getContextPath()%>/ServletDeleteResponseByOne">
                <button class="clear__response"  name="Id" value="<%=responses.get(i).getId()%>">
                    ������
                </button>
                </form>

            </div>

            <%if(responses.size() > i + 1){%>
            <div class="responses__inner__blocks">

                <h4 class="response__name__surname">
                    <%=responses.get(i + 1).getSurname()%> <%=responses.get(i + 1).getName()%>
                </h4>
                <p class="response__phone">
                    <%=responses.get(i + 1).getNumber()%>
                </p>
                <p class="response__vacancy__title">
                    <%=responses.get(i + 1).getJob_title()%>
                </p>
                <p class="response__company">
                    <%=responses.get(i + 1).getCompany_title()%>
                </p>
                <p class="response__city">
                    <%=responses.get(i + 1).getJober_city()%>
                </p>
                <form method="post" action="<%=request.getContextPath()%>/ServletDeleteResponseByOne">
                <button class="clear__response" name="Id" value="<%=responses.get(i + 1).getId()%>">
                    ������
                </button>
                </form>
            </div>
            <%}%>

            <%if(responses.size() > i + 2){%>
            <div class="responses__inner__blocks">

                <h4 class="response__name__surname">
                    <%=responses.get(i + 2).getSurname()%> <%=responses.get(i + 2).getName()%>
                </h4>
                <p class="response__phone">
                    <%=responses.get(i + 2).getNumber()%>
                </p>
                <p class="response__vacancy__title">
                    <%=responses.get(i + 2).getJob_title()%>
                </p>
                <p class="response__company">
                    <%=responses.get(i + 2).getCompany_title()%>
                </p>
                <p class="response__city">
                    <%=responses.get(i + 2).getJober_city()%>
                </p>
                <form method="post" action="<%=request.getContextPath()%>/ServletDeleteResponseByOne">
                <button class="clear__response" name="Id" value="<%=responses.get(i + 2).getId()%>">
                    ������
                </button>
                </form>
            </div>
            <%}%>

        </div>
        <%}%>


    </div>

</section>

</body>

</html>