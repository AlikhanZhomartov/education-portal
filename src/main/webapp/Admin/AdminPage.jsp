<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.edu.astanait.MakeResponse.MakeResponse" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/AdminPage.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</head>

<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>

        </div>
        <nav class="header__menu">
            <ul class="menu__list">
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>
            </ul>

        </nav>


    </div>



</header>

<section class="job__vacancy">

    <div class="container">
        <div class="job__vacancy__inner">

            <c:forEach var="vacancy" items="${list}">
                <div class="job__vacancy__inner__block">
                    <div class="job__vacancy__title_salary">
                        <h3 class="job__vacancy_title">
                                ${vacancy.job_title}
                        </h3>
                        <span class="job__vacancy__salary">
                            ${vacancy.job_salary} KZT
                        </span>
                    </div>

                    <p class="job__vacancy__company">
                            ${vacancy.company_title}
                    </p>
                    <p class="job__vacancy__city">
                            ${vacancy.jober_city}
                    </p>
                    <p class="job__vacancy__description">
                            ${vacancy.job_description}
                    </p>

                    <div class="btn_list">
                        <button type="submit" class="job__vacancy__response"  name="job_btn" id="Create${vacancy.id}" value="${vacancy.id}">
                            ��������
                        </button>
                        <button type="submit" class="job__vacancy__response" name="job_btnDelete" id="Delete${vacancy.id}" value="${vacancy.id}">
                            ���������
                        </button>
                        <p class="already" id="ready${vacancy.id}" style="display: none">�������� ��������</p>
                        <p class="alreadyDelete" id="readyDelete${vacancy.id}" style="display: none">�������� ���������</p>
                    </div>

                </div>

                <script>

                    $(document).ready(function (){
                        $("#Delete${vacancy.id}").click(function () {

                            $.ajax({
                                type: "POST",
                                url: "<%=request.getContextPath()%>/ServletDeleteRequested",
                                data: {
                                    "job_btnDelete": $("#Delete${vacancy.id}").val(),
                                },
                                success: function(result){
                                    $("#Create${vacancy.id}").css('display', 'none');
                                    $("#Delete${vacancy.id}").css('display', 'none');
                                    $("#readyDelete${vacancy.id}").css('display', 'block');
                                }
                            });
                        });
                    });
                </script>

                <script>

                    $(document).ready(function (){
                        $("#Create${vacancy.id}").click(function () {

                       $.ajax({
                                type: "POST",
                                url: "<%=request.getContextPath()%>/ServletCreateRealVacancy",
                                data: {
                                    "job_btn": $("#Create${vacancy.id}").val(),
                                },
                                success: function(result){
                                    $("#Create${vacancy.id}").css('display', 'none');
                                    $("#Delete${vacancy.id}").css('display', 'none');
                                    $("#ready${vacancy.id}").css('display', 'block');
                                }
                            });
                   });
                    });

                </script>
            </c:forEach>

        </div>
    </div>
</section>

</body>

</html>