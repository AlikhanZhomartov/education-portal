package kz.edu.astanait.GetSession;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GetSession {

    public Integer GetIdSession(HttpServletRequest request, HttpServletResponse response){

        HttpSession session = request.getSession();
        Integer Id = 0;
        Id = (Integer) session.getAttribute("Id");

        return Id;

    }
    public Integer GetResult(HttpServletRequest request, HttpServletResponse response){

        HttpSession session = request.getSession();
        Integer result = 0;
        result = (Integer) session.getAttribute("result");

        return result;

    }

    public Integer GetTest_id(HttpServletRequest request, HttpServletResponse response){

        HttpSession session = request.getSession();
        Integer test_id = 0;
        test_id = (Integer) session.getAttribute("test_id");

        return test_id;

    }




}
