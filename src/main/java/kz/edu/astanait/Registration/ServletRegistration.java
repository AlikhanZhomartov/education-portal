package kz.edu.astanait.Registration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletRegistration")
public class ServletRegistration extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

            request.setCharacterEncoding("Cp1251");

             String Email = request.getParameter("Email").trim();
             String Password = request.getParameter("Password").trim();
             String Name = request.getParameter("Name").trim();
             String Surname = request.getParameter("Surname").trim();
             String PersonType = request.getParameter("Gender");

             RegistrationDB db = new RegistrationDB();

             switch (PersonType){

                 case "schoolboy":
                      if(db.CheckingPasswordInStudent(Email, Password) && db.CheckingPasswordInEmployee(Email, Password)) {
                          db.RegisterSchoolboy(Email, Name, Surname, Password);
                      }

                 case "student":
                     if(db.CheckingPasswordInEmployee(Email, Password) && db.CheckingPasswordInSchoolboy(Email, Password)) {
                         db.RegisterStudent(Email, Name, Surname, Password);
                     }

                 case "employee":
                     if(db.CheckingPasswordInSchoolboy(Email, Password) && db.CheckingPasswordInStudent(Email, Password)) {
                         db.RegisterEmployee(Email, Name, Surname, Password);
                     }

             }

             request.getRequestDispatcher("General/Registration.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
