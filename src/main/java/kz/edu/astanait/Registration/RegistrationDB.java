package kz.edu.astanait.Registration;

import kz.edu.astanait.DBConnection.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RegistrationDB extends DBConnection {

protected void RegisterStudent(String Email, String Name, String Surname, String Password){
    Connection connection = null;
    PreparedStatement preparedStatement = null;

    try {
        connection = getConnection();
        preparedStatement = connection.prepareStatement("Insert into student(Student_email, Student_name, Student_surname, Student_password) value(?, ?, ?, ?)");

        preparedStatement.setString(1, Email);
        preparedStatement.setString(2, Name);
        preparedStatement.setString(3, Surname);
        preparedStatement.setString(4, Password);

        preparedStatement.executeUpdate();

        preparedStatement.close();
        connection.close();
    }
    catch (SQLException e){e.printStackTrace();}
}

    protected boolean CheckingPasswordInStudent(String Email, String Password){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select count(Id) as count from student where Student_email =? and Student_password = ?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            if(resultSet.getInt("count") == 0){
                checking = true;
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return checking;
    }



    protected void RegisterSchoolboy(String Email, String Name, String Surname, String Password){
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Insert into schoolboy (Schoolboy_email, Schoolboy_name, Schoolboyt_surname, Schoolboy_password) value(?, ?, ?, ?)");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Name);
            preparedStatement.setString(3, Surname);
            preparedStatement.setString(4, Password);

            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}
    }

    protected boolean CheckingPasswordInSchoolboy(String Email, String Password){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select count(Id) as count from schoolboy where Schoolboy_email =? and Schoolboy_password = ?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            if(resultSet.getInt("count") == 0){
                checking = true;
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return checking;
    }

    protected void RegisterEmployee(String Email, String Name, String Surname, String Password){
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Insert into employee(employee_email, employee_name, employee_surname, employee_password) value(?, ?, ?, ?)");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Name);
            preparedStatement.setString(3, Surname);
            preparedStatement.setString(4, Password);

            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}
    }

    protected boolean CheckingPasswordInEmployee(String Email, String Password){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select count(Id) as count from employee where Employee_email =? and Employee_password = ?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            if(resultSet.getInt("count") == 0){
                checking = true;
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return checking;
    }

}
