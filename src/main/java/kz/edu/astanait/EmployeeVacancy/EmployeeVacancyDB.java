package kz.edu.astanait.EmployeeVacancy;

import kz.edu.astanait.DBConnection.DBConnection;
import kz.edu.astanait.ListOfVacancy.ListOfVacancy;

import java.sql.*;
import java.util.ArrayList;

public class EmployeeVacancyDB extends DBConnection {

    protected ArrayList<EmployeeVacancy> SelectVacanciesForEmployee(int Employee_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        ArrayList<EmployeeVacancy> employeeVacancy = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select Id, Job_title, Company_title, Jober_city, Job_salary, Job_description from vacancy where Employee_id =? order by Id desc");
            preparedStatement.setInt(1, Employee_id);
            resultSet = preparedStatement.executeQuery();


            while (resultSet.next()){
                EmployeeVacancy employeeVacancy1 = new EmployeeVacancy();
                employeeVacancy1.setId(resultSet.getInt("Id"));
                employeeVacancy1.setJob_title(resultSet.getString("Job_title"));
                employeeVacancy1.setCompany_title(resultSet.getString("Company_title"));
                employeeVacancy1.setJober_city(resultSet.getString("Jober_city"));
                employeeVacancy1.setJob_salary(resultSet.getInt("Job_salary"));
                employeeVacancy1.setJob_description(resultSet.getString("Job_description"));

                employeeVacancy.add(employeeVacancy1);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return employeeVacancy;
    }


    protected void DeleteEmployeesVacancy(int Vacancy_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("delete from vacancy where Id =?");
            preparedStatement.setInt(1, Vacancy_id);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

    }
    protected void DeleteResponses(int Vacancy_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("delete from responses where Vacancy_id =?");
            preparedStatement.setInt(1, Vacancy_id);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

    }
}
