package kz.edu.astanait.EmployeeVacancy;

import kz.edu.astanait.ListOfVacancy.ServletListOfVacancyForEmployee;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletVacancyForEmployee")
public class ServletVacancyForEmployee extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int Id = Integer.parseInt(request.getParameter("Id"));

        EmployeeVacancyDB db = new EmployeeVacancyDB();
        db.DeleteResponses(Id);
        db.DeleteEmployeesVacancy(Id);
        ServletEmployeeVacancy sv = new ServletEmployeeVacancy();
        sv.doPost(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
