package kz.edu.astanait.EmployeeVacancy;

import kz.edu.astanait.GetSession.GetSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletEmployeeVacancy")
public class ServletEmployeeVacancy extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        EmployeeVacancyDB db = new EmployeeVacancyDB();
        GetSession gs = new GetSession();
        ArrayList<EmployeeVacancy> employeeVacancies = db.SelectVacanciesForEmployee(gs.GetIdSession(request, response));
        
        request.setAttribute("EmployeesVacancy", employeeVacancies);
        request.getRequestDispatcher("Employee/EmployeesVacancy.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
