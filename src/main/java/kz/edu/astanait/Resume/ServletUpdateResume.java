package kz.edu.astanait.Resume;

import kz.edu.astanait.GetSession.GetSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletUpdateResume")
public class ServletUpdateResume extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("Cp1251");

        ResumeDB db = new ResumeDB();
        GetSession gs = new GetSession();

        String name = request.getParameter("name").trim();
        String surname = request.getParameter("surname").trim();
        String mobile = request.getParameter("mobile").trim();
        String city = request.getParameter("city").trim();

        if(!name.equals("") && !surname.equals("") && !mobile.equals("") && !city.equals("")) {
            db.UpdateResume(name, surname, mobile, city, gs.GetIdSession(request, response));
            ServletResumeSelect sv = new ServletResumeSelect();
            sv.doPost(request, response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
