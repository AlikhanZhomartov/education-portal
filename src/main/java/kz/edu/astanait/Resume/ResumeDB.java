package kz.edu.astanait.Resume;

import kz.edu.astanait.DBConnection.DBConnection;
import kz.edu.astanait.ListOfVacancy.ListOfVacancy;

import java.sql.*;
import java.util.ArrayList;

public class ResumeDB extends DBConnection {


    protected ArrayList<Resume> SelectResume(int Id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Resume> resume = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select Id, Student_name, Student_surname, Student_mobile, Student_city from resume where Student_id = ?");

            preparedStatement.setInt(1, Id);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Resume resume1 = new Resume();
                resume1.setId(resultSet.getInt("Id"));
                resume1.setStudent_name(resultSet.getString("Student_name"));
                resume1.setStudent_surname(resultSet.getString("Student_surname"));
                resume1.setStudent_mobile(resultSet.getString("Student_mobile"));
                resume1.setStudent_city(resultSet.getString("Student_city"));

                resume.add(resume1);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return resume;
    }

    protected void UpdateResume(String Name, String Surname, String Mobile, String City, int Id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("UPDATE resume set Student_name = ?, Student_surname=?, Student_mobile =?, Student_city =?  where Student_id = ?");
            preparedStatement.setString(1, Name);
            preparedStatement.setString(2, Surname);
            preparedStatement.setString(3, Mobile);
            preparedStatement.setString(4, City);
            preparedStatement.setInt(5, Id);

            preparedStatement.executeUpdate();

            connection.close();
            preparedStatement.close();
        }
        catch (SQLException e){e.printStackTrace();}


    }

    protected void CreatResume(String Name, String Surname, String Mobile, String City, int Id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("insert into resume(Student_name, Student_surname, Student_mobile, Student_city, Student_id) value (?, ?, ?, ?, ?)");
            preparedStatement.setString(1, Name);
            preparedStatement.setString(2, Surname);
            preparedStatement.setString(3, Mobile);
            preparedStatement.setString(4, City);
            preparedStatement.setInt(5, Id);

            preparedStatement.executeUpdate();

            connection.close();
            preparedStatement.close();
        }
        catch (SQLException e){e.printStackTrace();}


    }

    protected int CheckResume(int Id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int count = 0;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select count(Id) as count from resume where Student_id = ?");
            preparedStatement.setInt(1, Id);

            resultSet = preparedStatement.executeQuery();

            resultSet.next();
            count = resultSet.getInt("count");

            resultSet.close();
            connection.close();
            preparedStatement.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return count;

    }

}
