package kz.edu.astanait.Resume;

public class Resume {

    private int Id;
    private String Student_name;
    private String Student_surname;
    private String Student_mobile;
    private String Student_city;

    public Resume() {}


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getStudent_name() {
        return Student_name;
    }

    public void setStudent_name(String student_name) {
        Student_name = student_name;
    }

    public String getStudent_surname() {
        return Student_surname;
    }

    public void setStudent_surname(String student_surname) {
        Student_surname = student_surname;
    }

    public String getStudent_mobile() {
        return Student_mobile;
    }

    public void setStudent_mobile(String student_mobile) {
        Student_mobile = student_mobile;
    }

    public String getStudent_city() {
        return Student_city;
    }

    public void setStudent_city(String student_city) {
        Student_city = student_city;
    }
}
