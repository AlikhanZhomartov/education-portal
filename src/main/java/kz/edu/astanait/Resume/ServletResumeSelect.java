package kz.edu.astanait.Resume;

import kz.edu.astanait.GetSession.GetSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletResumeSelect")
public class ServletResumeSelect extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      ResumeDB db = new ResumeDB();
      GetSession gs = new GetSession();
      ArrayList<Resume> resumes = db.SelectResume(gs.GetIdSession(request, response));
      int count = db.CheckResume(gs.GetIdSession(request, response));

      request.setAttribute("resume", resumes);
      request.setAttribute("count", count);
      request.getRequestDispatcher("Student/CreatResume.jsp").forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
