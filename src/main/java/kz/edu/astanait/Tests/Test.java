package kz.edu.astanait.Tests;

public class Test {
    private int Question_id;
    private String Question;
    private int Option_id;
    private String Option_text;
    private int Truth;

    public Test() {}

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getOption_text() {
        return Option_text;
    }

    public void setOption_text(String option_text) {
        Option_text = option_text;
    }

    public int getTruth() {
        return Truth;
    }

    public void setTruth(int truth) {
        Truth = truth;
    }

    public int getQuestion_id() {
        return Question_id;
    }

    public void setQuestion_id(int question_id) {
        Question_id = question_id;
    }

    public int getOption_id() {
        return Option_id;
    }

    public void setOption_id(int option_id) {
        Option_id = option_id;
    }
}
