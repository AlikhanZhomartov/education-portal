package kz.edu.astanait.Tests;

import kz.edu.astanait.DBConnection.DBConnection;
import kz.edu.astanait.ListOfVacancy.ListOfVacancy;

import java.sql.*;
import java.util.ArrayList;

public class TestDB extends DBConnection {


    protected ArrayList<TestList> SelectTestsList(){

        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        ArrayList<TestList> tests = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from test");

            while (resultSet.next()){
                TestList testList = new TestList();

                testList.setId(resultSet.getInt("Id"));
                testList.setTitle(resultSet.getString("Title"));
                testList.setImage(resultSet.getString("Images"));
                tests.add(testList);

            }
            resultSet.close();
            statement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return tests;
    }

    protected ArrayList<Test> SelectTest(int Test_id){

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Test> tests = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select questions.Id, questions.Question, options.Id, options.Option_text, options.Truth from questions inner join test on questions.Test_id = test.Id inner join options on questions.Id = options.Question_id where test.Id = ?;");

            preparedStatement.setInt(1, Test_id);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Test test = new Test();
                test.setQuestion_id(resultSet.getInt("questions.Id"));
                test.setQuestion(resultSet.getString("Question"));
                test.setOption_id(resultSet.getInt("options.Id"));
                test.setOption_text(resultSet.getString("Option_text"));
                test.setTruth(resultSet.getInt("Truth"));

                tests.add(test);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return tests;
    }

}
