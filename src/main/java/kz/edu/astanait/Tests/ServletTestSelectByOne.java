package kz.edu.astanait.Tests;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletTestSelectByOne")
public class ServletTestSelectByOne extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int Id = Integer.parseInt(request.getParameter("sbm_btn"));
        TestDB db = new TestDB();

        HttpSession session = request.getSession();
        session.setAttribute("result", 0);
        session.setMaxInactiveInterval(-1);

        session.setAttribute("test_id", Id);
        session.setMaxInactiveInterval(-1);

        ArrayList<Test> test = db.SelectTest(Id);
        request.setAttribute("tests", test);
        request.getRequestDispatcher("Schoolboy/Test.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
