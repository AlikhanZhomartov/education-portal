package kz.edu.astanait.Tests;

import kz.edu.astanait.GetSession.GetSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletCountResult")
public class ServletCountResult extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        GetSession gs = new GetSession();

        int GetResult = Integer.parseInt(request.getParameter("result"));
        int FinalResult = gs.GetResult(request, response) + GetResult;


        HttpSession session = request.getSession();
        session.setAttribute("result", FinalResult);
        session.setMaxInactiveInterval(-1);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
