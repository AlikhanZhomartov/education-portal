package kz.edu.astanait.Responses;

import kz.edu.astanait.DBConnection.DBConnection;
import kz.edu.astanait.EmployeeVacancy.EmployeeVacancy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ResponsesDB extends DBConnection {

    protected ArrayList<Responses> SelectResponsesForEmployee(int Employee_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        ArrayList<Responses> responses = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select responses.Id, resume.Student_name, resume.Student_surname, resume.Student_mobile, vacancy.Job_title, vacancy.Company_title, resume.Student_city from responses inner join resume on responses.Resume_id = resume.Id inner join vacancy on responses.Vacancy_id = vacancy.Id inner join employee on vacancy.Employee_id = employee.Id where employee.Id = ? order by responses.Id desc");
            preparedStatement.setInt(1, Employee_id);
            resultSet = preparedStatement.executeQuery();


            while (resultSet.next()){
                Responses responses1 = new Responses();
                responses1.setId(resultSet.getInt("Id"));
                responses1.setName(resultSet.getString("Student_name"));
                responses1.setSurname(resultSet.getString("Student_surname"));
                responses1.setNumber(resultSet.getString("Student_mobile"));
                responses1.setJob_title(resultSet.getString("Job_title"));
                responses1.setCompany_title(resultSet.getString("Company_title"));
                responses1.setJober_city(resultSet.getString("Student_city"));

                responses.add(responses1);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return responses;
    }

    protected void DeleteResponses(int Employee_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        ArrayList<EmployeeVacancy> employeeVacancy = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("delete responses from responses inner join vacancy on responses.Vacancy_id = vacancy.Id where vacancy.Employee_id = ?");
            preparedStatement.setInt(1, Employee_id);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

    }

    protected void DeleteResponseByOne(int Response_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        ArrayList<EmployeeVacancy> employeeVacancy = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("delete from responses where Id = ?");
            preparedStatement.setInt(1, Response_id);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

    }
}
