package kz.edu.astanait.Responses;

import kz.edu.astanait.GetSession.GetSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletSelectResponses")
public class ServletSelectResponses extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ResponsesDB db = new ResponsesDB();
        GetSession gs = new GetSession();

        ArrayList<Responses> responses = db.SelectResponsesForEmployee(gs.GetIdSession(request, response));
        request.setAttribute("responses", responses);
        request.getRequestDispatcher("Employee/Responses.jsp").forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
