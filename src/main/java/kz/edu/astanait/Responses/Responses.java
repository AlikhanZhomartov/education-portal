package kz.edu.astanait.Responses;

public class Responses {

    private int Id;
    private String Name;
    private String Surname;
    private String Number;
    private String Job_title;
    private String Company_title;
    private String Jober_city;

    public Responses() {}


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getJob_title() {
        return Job_title;
    }

    public void setJob_title(String job_title) {
        Job_title = job_title;
    }

    public String getCompany_title() {
        return Company_title;
    }

    public void setCompany_title(String company_title) {
        Company_title = company_title;
    }

    public String getJober_city() {
        return Jober_city;
    }

    public void setJober_city(String jober_city) {
        Jober_city = jober_city;
    }
}
