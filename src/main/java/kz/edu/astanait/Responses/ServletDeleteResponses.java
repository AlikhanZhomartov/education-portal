package kz.edu.astanait.Responses;

import kz.edu.astanait.GetSession.GetSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletDeleteResponses")
public class ServletDeleteResponses extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        ResponsesDB db = new ResponsesDB();
        GetSession gs  = new GetSession();
        db.DeleteResponses(gs.GetIdSession(request, response));
        ServletSelectResponses sv = new ServletSelectResponses();
        sv.doPost(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
