package kz.edu.astanait.ChangePassword;

import kz.edu.astanait.GetSession.GetSession;

import javax.enterprise.inject.New;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletChangePassword")
public class ServletChangePassword extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

         request.setCharacterEncoding("Cp1251");

         ChangePasswordDB db = new ChangePasswordDB();
         GetSession gs = new GetSession();
         String Email = request.getParameter("Email").trim();
         String OldPassword = request.getParameter("OldPassword").trim();
         String NewPassword = request.getParameter("NewPassword").trim();
         String RepeatedPassword = request.getParameter("RepeatedPassword").trim();

         boolean CheckStudent = db.CheckPasswordStudent(gs.GetIdSession(request, response), OldPassword, Email);
         boolean CheckSchoolboy = db.CheckPasswordSchoolboy(gs.GetIdSession(request, response), OldPassword, Email);
         boolean CheckEmployee = db.CheckPasswordEmployee(gs.GetIdSession(request, response), OldPassword, Email);

         if(CheckStudent && NewPassword.equals(RepeatedPassword) && db.CheckingPasswordInEmployee(Email, NewPassword) && db.CheckingPasswordInSchoolboy(Email, NewPassword)){
             db.ChangePasswordStudent(gs.GetIdSession(request, response), NewPassword);

         }
         else if(CheckSchoolboy && NewPassword.equals(RepeatedPassword) && db.CheckingPasswordInStudent(Email, NewPassword) && db.CheckingPasswordInEmployee(Email, NewPassword)){
             db.ChangePasswordSchoolboy(gs.GetIdSession(request, response), NewPassword);
         }
         else if(CheckEmployee && NewPassword.equals(RepeatedPassword) && db.CheckingPasswordInStudent(Email, NewPassword) && db.CheckingPasswordInSchoolboy(Email, NewPassword)){
             db.ChangePasswordEmployee(gs.GetIdSession(request, response), NewPassword);

         }



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
