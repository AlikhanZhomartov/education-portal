package kz.edu.astanait.ChangePassword;

import kz.edu.astanait.DBConnection.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ChangePasswordDB extends DBConnection {

    protected boolean CheckPasswordStudent(int Id, String Password, String Email) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT count(Id) as count FROM student WHERE Id=? and Student_password=? and Student_email = ?");

            preparedStatement.setInt(1, Id);
            preparedStatement.setString(2, Password);
            preparedStatement.setString(3, Email);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            checking = resultSet.getInt("count") != 0;

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return checking;

    }


    protected void ChangePasswordStudent(int Id,String Password){

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("UPDATE student set Student_password = ? where Id = ?");
            preparedStatement.setString(1, Password);
            preparedStatement.setInt(2, Id);

            preparedStatement.executeUpdate();

            connection.close();
            preparedStatement.close();
        }
        catch (SQLException e){e.printStackTrace();}


    }

    protected boolean CheckingPasswordInStudent(String Email, String Password){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select count(Id) as count from student where Student_email =? and Student_password = ?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            if(resultSet.getInt("count") == 0){
                checking = true;
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return checking;
    }

    protected boolean CheckPasswordSchoolboy(int Id, String Password, String Email) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT count(Id) as count FROM schoolboy WHERE Id=? and Schoolboy_password=? and Schoolboy_email = ?");

            preparedStatement.setInt(1, Id);
            preparedStatement.setString(2, Password);
            preparedStatement.setString(3, Email);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            checking = resultSet.getInt("count") != 0;

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return checking;

    }


    protected void ChangePasswordSchoolboy(int Id,String Password){

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("UPDATE schoolboy set Schoolboy_password = ? where Id = ?");
            preparedStatement.setString(1, Password);
            preparedStatement.setInt(2, Id);

            preparedStatement.executeUpdate();

            connection.close();
            preparedStatement.close();
        }
        catch (SQLException e){e.printStackTrace();}


    }

    protected boolean CheckingPasswordInSchoolboy(String Email, String Password){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select count(Id) as count from schoolboy where Schoolboy_email =? and Schoolboy_password = ?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            if(resultSet.getInt("count") == 0){
                checking = true;
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return checking;
    }

    protected boolean CheckPasswordEmployee(int Id, String Password, String Email) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT count(Id) as count FROM employee WHERE Id=? and Employee_password=? and Employee_email = ?");

            preparedStatement.setInt(1, Id);
            preparedStatement.setString(2, Password);
            preparedStatement.setString(3, Email);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            checking = resultSet.getInt("count") != 0;

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return checking;

    }


    protected void ChangePasswordEmployee(int Id,String Password){

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("UPDATE employee set Employee_password = ? where Id = ?");
            preparedStatement.setString(1, Password);
            preparedStatement.setInt(2, Id);

            preparedStatement.executeUpdate();

            connection.close();
            preparedStatement.close();
        }
        catch (SQLException e){e.printStackTrace();}


    }
    protected boolean CheckingPasswordInEmployee(String Email, String Password){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean checking = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select count(Id) as count from employee where Employee_email =? and Employee_password = ?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            if(resultSet.getInt("count") == 0){
                checking = true;
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return checking;
    }


}
