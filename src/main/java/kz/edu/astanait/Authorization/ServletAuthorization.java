package kz.edu.astanait.Authorization;

import kz.edu.astanait.AdminCreateVacancies.ServletSelectVacancyForAdmin;
import kz.edu.astanait.ListOfVacancy.ServletListOfVacancy;
import kz.edu.astanait.ListOfVacancy.ServletListOfVacancyForEmployee;
import kz.edu.astanait.Tests.ServletTestsSelect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ServletAuthorization")
public class ServletAuthorization extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("Cp1251");

        String Email = request.getParameter("Email").trim();
        String Password = request.getParameter("Password").trim();
        AuthorizationDB db = new AuthorizationDB();


        boolean checkStudent = db.CheckStudent(Email, Password);
        boolean checkSchoolboy = db.CheckSchoolboy(Email, Password);
        boolean checkEmployee = db.CheckEmployee(Email, Password);
        boolean checkAdmin = db.CheckAdmin(Email, Password);

        if(checkStudent){

            HttpSession session = request.getSession();
            session.setAttribute("Id", db.SelectPasswordAndEmailForStudent(Email, Password));
            session.setMaxInactiveInterval(-1);

            ServletListOfVacancy sv = new ServletListOfVacancy();
            sv.doPost(request, response);

        }else if(checkSchoolboy){

            HttpSession session = request.getSession();
            session.setAttribute("Id", db.SelectPasswordAndEmailForSchoolboy(Email, Password));
            session.setMaxInactiveInterval(-1);

            ServletTestsSelect ser = new ServletTestsSelect();
            ser.doPost(request, response);

        }else if(checkEmployee){

            HttpSession session = request.getSession();
            session.setAttribute("Id", db.SelectPasswordAndEmailForEmployee(Email, Password));
            session.setMaxInactiveInterval(-1);

            ServletListOfVacancyForEmployee sv = new ServletListOfVacancyForEmployee();
            sv.doPost(request, response);

        }else if(checkAdmin){
            HttpSession session = request.getSession();
            session.setAttribute("Id", db.SelectPasswordAndEmailForAdmin(Email, Password));
            session.setMaxInactiveInterval(-1);

            ServletSelectVacancyForAdmin sv = new ServletSelectVacancyForAdmin();
            sv.doPost(request, response);
        }
        else{
            request.getRequestDispatcher("General/Authorization.jsp").forward(request, response);
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
