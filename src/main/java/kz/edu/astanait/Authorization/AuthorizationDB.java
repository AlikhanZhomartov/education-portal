package kz.edu.astanait.Authorization;

import kz.edu.astanait.DBConnection.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorizationDB extends DBConnection {

    protected int SelectPasswordAndEmailForStudent(String Email, String Password) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int id = 0;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT Id FROM student WHERE Student_email=? and Student_password=?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            id = resultSet.getInt("Id");

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return id;

    }

    protected int SelectPasswordAndEmailForSchoolboy(String Email, String Password) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int id = 0;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT Id FROM schoolboy WHERE Schoolboy_email=? and Schoolboy_password=?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            id = resultSet.getInt("Id");

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return id;

    }

    protected int SelectPasswordAndEmailForEmployee(String Email, String Password) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int id = 0;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT Id FROM employee WHERE Employee_email=? and Employee_password=?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            id = resultSet.getInt("Id");

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return id;

    }

    protected  int SelectPasswordAndEmailForAdmin(String Email, String Password) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int id = 0;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT Id FROM admin WHERE Admin_email=? and Admin_password=?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            id = resultSet.getInt("Id");

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return id;

    }


    protected boolean CheckStudent(String Email, String Password) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean check = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT count(Student_email) as count FROM student WHERE Student_email=? and Student_password=?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            check = resultSet.getInt("count") != 0;

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return check;

    }

    protected boolean CheckSchoolboy(String Email, String Password) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean check = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT count(Schoolboy_email) as count FROM schoolboy WHERE Schoolboy_email=? and Schoolboy_password=?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            check = resultSet.getInt("count") != 0;

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return check;

    }

    protected boolean CheckEmployee(String Email, String Password) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean check = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT count(Employee_email) as count FROM employee WHERE Employee_email=? and Employee_password=?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            check = resultSet.getInt("count") != 0;

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return check;

    }

    protected  boolean CheckAdmin(String Email, String Password) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean check = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("SELECT count(Admin_email) as count FROM admin WHERE Admin_email=? and Admin_password=?");

            preparedStatement.setString(1, Email);
            preparedStatement.setString(2, Password);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();


            check = resultSet.getInt("count") != 0;

            resultSet.close();
            preparedStatement.close();
            connection.close();

        }
        catch (SQLException e){e.printStackTrace();}

        return check;

    }

}
