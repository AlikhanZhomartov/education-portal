package kz.edu.astanait.MakeResponse;

import kz.edu.astanait.GetSession.GetSession;
import kz.edu.astanait.ListOfVacancy.ServletListOfVacancy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletMakeResponse")
public class ServletMakeResponse extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        MakeResponseDB db = new MakeResponseDB();
        GetSession gs = new GetSession();
        int resume_id = db.SelectResume_id(gs.GetIdSession(request, response));
        int vacancy_id = Integer.parseInt(request.getParameter("job_btn"));

        if (db.CheckResume(resume_id)) {

            db.MakeResponse(vacancy_id, resume_id);

        }




    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
