package kz.edu.astanait.MakeResponse;

import kz.edu.astanait.DBConnection.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MakeResponseDB extends DBConnection {

    protected void MakeResponse(int Vacancy_id, int Resume_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Insert into responses (Vacancy_id, Resume_id) value(?, ?)");

            preparedStatement.setInt(1, Vacancy_id);
            preparedStatement.setInt(2, Resume_id);

            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}
    }

    protected int SelectResume_id(int Student_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int Id = 0;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Select Id from resume where Student_id = ?");

            preparedStatement.setInt(1, Student_id);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            Id = resultSet.getInt("Id");

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return Id;
    }

    protected ArrayList<MakeResponse> CheckForResponse(int Student_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<MakeResponse> makeResponses = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select responses.Vacancy_id from responses inner join resume on responses.Resume_id = resume.Id where resume.Student_id = ?");

            preparedStatement.setInt(1, Student_id);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                MakeResponse makeResponse1 = new MakeResponse();

                makeResponse1.setVacancy_id(resultSet.getInt("Vacancy_id"));

                makeResponses.add(makeResponse1);
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return makeResponses;
    }


    protected boolean CheckResume(int Resume_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean check = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Select count(Id) as count from resume where Id = ?");

            preparedStatement.setInt(1, Resume_id);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            check = resultSet.getInt("count") != 0;

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return check;
    }


}
