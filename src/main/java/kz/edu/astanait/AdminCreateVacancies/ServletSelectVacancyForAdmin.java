package kz.edu.astanait.AdminCreateVacancies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletSelectVacancyForAdmin")
public class ServletSelectVacancyForAdmin extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        CreateVacancy db = new CreateVacancy();
        ArrayList<SelectRequestedVacancy> arrayList = db.SelectVacancies();
        request.setAttribute("list", arrayList);
        request.getRequestDispatcher("Admin/AdminPage.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
