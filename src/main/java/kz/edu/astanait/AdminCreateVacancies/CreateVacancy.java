package kz.edu.astanait.AdminCreateVacancies;

import kz.edu.astanait.DBConnection.DBConnection;
import kz.edu.astanait.ListOfVacancy.ListOfVacancy;

import java.sql.*;
import java.util.ArrayList;

public class CreateVacancy extends DBConnection {

    protected ArrayList<SelectRequestedVacancy> SelectVacancies(){

        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        ArrayList<SelectRequestedVacancy> selectRequestedVacancies = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select Id, Job_title, Company_title, Jober_city, Job_salary, Job_description from requested_vacancy");

            while (resultSet.next()){

                SelectRequestedVacancy selectRequestedVacancy = new SelectRequestedVacancy();
                selectRequestedVacancy.setId(resultSet.getInt("Id"));
                selectRequestedVacancy.setJob_title(resultSet.getString("Job_title"));
                selectRequestedVacancy.setCompany_title(resultSet.getString("Company_title"));
                selectRequestedVacancy.setJober_city(resultSet.getString("Jober_city"));
                selectRequestedVacancy.setJob_salary(resultSet.getInt("Job_salary"));
                selectRequestedVacancy.setJob_description(resultSet.getString("Job_description"));

                selectRequestedVacancies.add(selectRequestedVacancy);

            }
            resultSet.close();
            statement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return selectRequestedVacancies;
    }

    protected void DeleteVacancy(int Vacancy_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("delete from requested_vacancy where Id =?");
            preparedStatement.setInt(1, Vacancy_id);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

    }


    protected ArrayList<String> SelectVacancyById(int Id){

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        ArrayList<String> arrayList = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select Job_title, Company_title, Jober_city, Job_salary, Job_description, Employee_id from requested_vacancy where Id = ?");
            preparedStatement.setInt(1,Id);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                arrayList.add(resultSet.getString("Job_title"));
                arrayList.add(resultSet.getString("Company_title"));
                arrayList.add(resultSet.getString("Jober_city"));
                arrayList.add(resultSet.getString("Job_salary"));
                arrayList.add(resultSet.getString("Job_description"));
                arrayList.add(resultSet.getString("Employee_id"));
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return arrayList;
    }

    protected void CreateRealVacancy(String Job_title, String Company_title, String Jober_city, int Job_salary, String Job_description, int Employee_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Insert into vacancy (Job_title, Company_title, Jober_city, Job_salary, Job_description, Employee_id) value(?, ?, ?, ?, ?, ?)");

            preparedStatement.setString(1, Job_title);
            preparedStatement.setString(2, Company_title);
            preparedStatement.setString(3, Jober_city);
            preparedStatement.setInt(4, Job_salary);
            preparedStatement.setString(5, Job_description);
            preparedStatement.setInt(6, Employee_id);

            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}
    }

}
