package kz.edu.astanait.AdminCreateVacancies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletCreateRealVacancy")
public class ServletCreateRealVacancy extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int Id = Integer.parseInt(request.getParameter("job_btn"));
        CreateVacancy db = new CreateVacancy();
        ArrayList<String> arrayList = db.SelectVacancyById(Id);



        db.CreateRealVacancy(arrayList.get(0), arrayList.get(1), arrayList.get(2), Integer.parseInt(arrayList.get(3)), arrayList.get(4), Integer.parseInt(arrayList.get(5)));
        db.DeleteVacancy(Id);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
