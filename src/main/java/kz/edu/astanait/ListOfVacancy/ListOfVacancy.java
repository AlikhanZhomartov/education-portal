package kz.edu.astanait.ListOfVacancy;

public class ListOfVacancy {

    private int Id;
    private String Job_title;
    private String Company_title;
    private String Jober_city;
    private int Job_salary;
    private String Job_description;

    public ListOfVacancy() {}


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getJob_title() {
        return Job_title;
    }

    public void setJob_title(String job_title) {
        Job_title = job_title;
    }

    public String getCompany_title() {
        return Company_title;
    }

    public void setCompany_title(String company_title) {
        Company_title = company_title;
    }

    public String getJober_city() {
        return Jober_city;
    }

    public void setJober_city(String jober_city) {
        Jober_city = jober_city;
    }

    public int getJob_salary() {
        return Job_salary;
    }

    public void setJob_salary(int job_salary) {
        Job_salary = job_salary;
    }

    public String getJob_description() {
        return Job_description;
    }

    public void setJob_description(String job_description) {
        Job_description = job_description;
    }
}
