package kz.edu.astanait.ListOfVacancy;

import kz.edu.astanait.GetSession.GetSession;
import kz.edu.astanait.MakeResponse.MakeResponse;
import kz.edu.astanait.MakeResponse.MakeResponseDB;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletListOfVacancy")
public class ServletListOfVacancy extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      ListOfVacancyDB db = new ListOfVacancyDB();
      GetSession gs = new GetSession();
      ArrayList<ListOfVacancy> listOfVacancies = db.SelectVacancies();
      ArrayList<MakeResponse> makeResponses = db.CheckForResponse(gs.GetIdSession(request, response));
      int resume_id = db.SelectResume_id(gs.GetIdSession(request, response));
      boolean check = db.CheckResume(resume_id);

      request.setAttribute("Vacancies", listOfVacancies);
      request.setAttribute("CheckResponse", makeResponses);
      request.setAttribute("checkResume", check);
      request.getRequestDispatcher("Student/ListOfVacancyForStudent.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
