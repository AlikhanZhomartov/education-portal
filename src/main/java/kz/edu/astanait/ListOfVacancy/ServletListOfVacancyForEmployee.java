package kz.edu.astanait.ListOfVacancy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletListOfVacancyForEmployee")
public class ServletListOfVacancyForEmployee extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ListOfVacancyDB db = new ListOfVacancyDB();
        ArrayList<ListOfVacancy> listOfVacancies = db.SelectVacancies();

        request.setAttribute("Vacancies", listOfVacancies);
        request.getRequestDispatcher("Employee/VacancyListForEmployee.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
