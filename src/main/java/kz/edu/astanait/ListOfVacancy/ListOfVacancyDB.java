package kz.edu.astanait.ListOfVacancy;

import kz.edu.astanait.DBConnection.DBConnection;
import kz.edu.astanait.MakeResponse.MakeResponse;

import java.sql.*;
import java.util.ArrayList;

public class ListOfVacancyDB extends DBConnection {

    protected ArrayList<ListOfVacancy> SelectVacancies(){

        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        ArrayList<ListOfVacancy> listOfVacancy = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select Id, Job_title, Company_title, Jober_city, Job_salary, Job_description from vacancy order by Id desc");

            while (resultSet.next()){
                ListOfVacancy listOfVacancy1 = new ListOfVacancy();
                listOfVacancy1.setId(resultSet.getInt("Id"));
                listOfVacancy1.setJob_title(resultSet.getString("Job_title"));
                listOfVacancy1.setCompany_title(resultSet.getString("Company_title"));
                listOfVacancy1.setJober_city(resultSet.getString("Jober_city"));
                listOfVacancy1.setJob_salary(resultSet.getInt("Job_salary"));
                listOfVacancy1.setJob_description(resultSet.getString("Job_description"));

                listOfVacancy.add(listOfVacancy1);

            }
            resultSet.close();
            statement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return listOfVacancy;
    }

    protected boolean CheckResume(int Resume_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean check = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Select count(Id) as count from resume where Id = ?");

            preparedStatement.setInt(1, Resume_id);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            check = resultSet.getInt("count") != 0;

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return check;
    }

    protected int SelectResume_id(int Student_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int Id = 0;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Select Id from resume where Student_id = ?");

            preparedStatement.setInt(1, Student_id);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            Id = resultSet.getInt("Id");

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return Id;
    }

    protected ArrayList<MakeResponse> CheckForResponse(int Student_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<MakeResponse> makeResponses = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select responses.Vacancy_id from responses inner join resume on responses.Resume_id = resume.Id where resume.Student_id = ?");

            preparedStatement.setInt(1, Student_id);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                MakeResponse makeResponse1 = new MakeResponse();

                makeResponse1.setVacancy_id(resultSet.getInt("Vacancy_id"));

                makeResponses.add(makeResponse1);
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return makeResponses;
    }


}
