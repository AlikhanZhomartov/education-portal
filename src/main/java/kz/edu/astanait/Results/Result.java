package kz.edu.astanait.Results;

public class Result {

    private String text;
    private String uni_title;
    private String uni_link;
    private String uni_type;
    private String uni_city;
    private String uni_description;

    public Result() {}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUni_title() {
        return uni_title;
    }

    public void setUni_title(String uni_title) {
        this.uni_title = uni_title;
    }

    public String getUni_link() {
        return uni_link;
    }

    public void setUni_link(String uni_link) {
        this.uni_link = uni_link;
    }

    public String getUni_type() {
        return uni_type;
    }

    public void setUni_type(String uni_type) {
        this.uni_type = uni_type;
    }

    public String getUni_city() {
        return uni_city;
    }

    public void setUni_city(String uni_city) {
        this.uni_city = uni_city;
    }

    public String getUni_description() {
        return uni_description;
    }

    public void setUni_description(String uni_description) {
        this.uni_description = uni_description;
    }
}
