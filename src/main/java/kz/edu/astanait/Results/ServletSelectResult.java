package kz.edu.astanait.Results;

import kz.edu.astanait.GetSession.GetSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletSelectResult")
public class ServletSelectResult extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        GetSession gs = new GetSession();
        ResultDB db = new ResultDB();
        int test_id = gs.GetTest_id(request, response);
        int score = gs.GetResult(request, response);

        ArrayList<Result> results = db.SelectResult(test_id, score);
        String result = results.get(0).getText();
        request.setAttribute("result", result);
        request.setAttribute("University", results);
        request.getRequestDispatcher("Schoolboy/Recommendation.jsp").forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
