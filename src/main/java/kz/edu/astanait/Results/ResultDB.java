package kz.edu.astanait.Results;

import kz.edu.astanait.DBConnection.DBConnection;
import kz.edu.astanait.ListOfVacancy.ListOfVacancy;

import java.sql.*;
import java.util.ArrayList;

public class ResultDB extends DBConnection {

    protected ArrayList<Result> SelectResult(int Id, int score){

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Result> results = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select recommendations.text, university.Title, university.Link, university.TypeOf, university.City, university.Description from recommendations inner join university on recommendations.Id = university.recommendation_id inner join test on recommendations.test_id = test.Id where test.Id = ? and recommendations.min_score <= ? and recommendations.max_score >= ?");

            preparedStatement.setInt(1, Id);
            preparedStatement.setInt(2, score);
            preparedStatement.setInt(3, score);

            resultSet = preparedStatement.executeQuery();


            while (resultSet.next()){
                Result result = new Result();
                result.setText(resultSet.getString("text"));
                result.setUni_title(resultSet.getString("Title"));
                result.setUni_link(resultSet.getString("Link"));
                result.setUni_type(resultSet.getString("TypeOf"));
                result.setUni_city(resultSet.getString("City"));
                result.setUni_description(resultSet.getString("Description"));

                results.add(result);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return results;
    }

}
