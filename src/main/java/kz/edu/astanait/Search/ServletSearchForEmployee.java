package kz.edu.astanait.Search;

import kz.edu.astanait.GetSession.GetSession;
import kz.edu.astanait.MakeResponse.MakeResponse;
import kz.edu.astanait.MakeResponse.MakeResponseDB;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletSearchForEmployee")
public class ServletSearchForEmployee extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("Cp1251");

        String content = request.getParameter("content");
        SearchDB db = new SearchDB();

        ArrayList<Search> searchByAll = db.SelectVacanciesByAll(content);
        ArrayList<Search> searchByTitleAndCity = db.SelectVacanciesByTitleAndCity(content);
        ArrayList<Search> searchByTitleAndCompany = db.SelectVacanciesByTitleAndCompany(content);
        ArrayList<Search> searchByCityAndCompany = db.SelectVacanciesByCityAndCompany(content);
        ArrayList<Search> searchByRandom = db.SelectVacanciesByRandom(content);

        if(searchByAll.size() != 0){

            request.setAttribute("result", searchByAll);

        }
        else if(searchByTitleAndCity.size() != 0){

            request.setAttribute("result", searchByTitleAndCity);

        }
        else if(searchByTitleAndCompany.size() != 0){

            request.setAttribute("result", searchByTitleAndCompany);

        }
        else if(searchByCityAndCompany.size() != 0){

            request.setAttribute("result", searchByCityAndCompany);

        }
        else if(searchByRandom.size() != 0){

            request.setAttribute("result", searchByRandom);

        }

        request.getRequestDispatcher("Employee/Search.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
