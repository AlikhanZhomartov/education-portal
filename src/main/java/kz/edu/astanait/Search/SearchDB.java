package kz.edu.astanait.Search;

import kz.edu.astanait.DBConnection.DBConnection;
import kz.edu.astanait.ListOfVacancy.ListOfVacancy;
import kz.edu.astanait.MakeResponse.MakeResponse;

import java.sql.*;
import java.util.ArrayList;

public class SearchDB extends DBConnection {


    protected ArrayList<Search> SelectVacanciesByAll(String content){

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Search> search = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select Id, Job_title, Company_title, Jober_city, Job_salary, Job_description from vacancy where Job_title like  ? and Jober_city like ?  and Company_title like ?");

            preparedStatement.setString(1, "%" + content + "%");
            preparedStatement.setString(2, "%" + content  + "%");
            preparedStatement.setString(3, "%" + content  + "%");


            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Search search1 = new Search();
                search1.setId(resultSet.getInt("Id"));
                search1.setJob_title(resultSet.getString("Job_title"));
                search1.setCompany_title(resultSet.getString("Company_title"));
                search1.setJober_city(resultSet.getString("Jober_city"));
                search1.setJob_salary(resultSet.getInt("Job_salary"));
                search1.setJob_description(resultSet.getString("Job_description"));

                search.add(search1);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return search;
    }

    protected ArrayList<Search> SelectVacanciesByTitleAndCity(String content){

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Search> search = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select Id, Job_title, Company_title, Jober_city, Job_salary, Job_description from vacancy where Job_title like  ? and Jober_city like  ?");

            preparedStatement.setString(1, "%" + content + "%");
            preparedStatement.setString(2, "%" + content  + "%");


            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Search search1 = new Search();
                search1.setId(resultSet.getInt("Id"));
                search1.setJob_title(resultSet.getString("Job_title"));
                search1.setCompany_title(resultSet.getString("Company_title"));
                search1.setJober_city(resultSet.getString("Jober_city"));
                search1.setJob_salary(resultSet.getInt("Job_salary"));
                search1.setJob_description(resultSet.getString("Job_description"));

                search.add(search1);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return search;
    }

    protected ArrayList<Search> SelectVacanciesByTitleAndCompany(String content){

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Search> search = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select Id, Job_title, Company_title, Jober_city, Job_salary, Job_description from vacancy where Job_title like ?  and Company_title like ? ");

            preparedStatement.setString(1, "%" + content + "%");
            preparedStatement.setString(2, "%" + content  + "%");


            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Search search1 = new Search();
                search1.setId(resultSet.getInt("Id"));
                search1.setJob_title(resultSet.getString("Job_title"));
                search1.setCompany_title(resultSet.getString("Company_title"));
                search1.setJober_city(resultSet.getString("Jober_city"));
                search1.setJob_salary(resultSet.getInt("Job_salary"));
                search1.setJob_description(resultSet.getString("Job_description"));

                search.add(search1);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return search;
    }

    protected ArrayList<Search> SelectVacanciesByCityAndCompany(String content){

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Search> search = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select Id, Job_title, Company_title, Jober_city, Job_salary, Job_description from vacancy where Jober_city like  ? and Company_title like ? ");

            preparedStatement.setString(1, "%" + content + "%");
            preparedStatement.setString(2, "%" + content  + "%");


            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Search search1 = new Search();
                search1.setId(resultSet.getInt("Id"));
                search1.setJob_title(resultSet.getString("Job_title"));
                search1.setCompany_title(resultSet.getString("Company_title"));
                search1.setJober_city(resultSet.getString("Jober_city"));
                search1.setJob_salary(resultSet.getInt("Job_salary"));
                search1.setJob_description(resultSet.getString("Job_description"));

                search.add(search1);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return search;
    }

    protected ArrayList<Search> SelectVacanciesByRandom(String content){

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Search> search = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select Id, Job_title, Company_title, Jober_city, Job_salary, Job_description from vacancy where Job_title like ? or Jober_city like ? or Company_title like ?");
            


            preparedStatement.setString(1, "%" + content + "%");
            preparedStatement.setString(2, "%" + content  + "%");
            preparedStatement.setString(3, "%" + content  + "%");



            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Search search1 = new Search();
                search1.setId(resultSet.getInt("Id"));
                search1.setJob_title(resultSet.getString("Job_title"));
                search1.setCompany_title(resultSet.getString("Company_title"));
                search1.setJober_city(resultSet.getString("Jober_city"));
                search1.setJob_salary(resultSet.getInt("Job_salary"));
                search1.setJob_description(resultSet.getString("Job_description"));

                search.add(search1);

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (SQLException e){e.printStackTrace();}

        return search;
    }

    protected ArrayList<MakeResponse> CheckForResponse(int Student_id){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<MakeResponse> makeResponses = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("select responses.Vacancy_id from responses inner join resume on responses.Resume_id = resume.Id where resume.Student_id = ?");

            preparedStatement.setInt(1, Student_id);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                MakeResponse makeResponse1 = new MakeResponse();

                makeResponse1.setVacancy_id(resultSet.getInt("Vacancy_id"));

                makeResponses.add(makeResponse1);
            }

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return makeResponses;
    }

    protected boolean CheckResume(int Resume_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean check = false;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Select count(Id) as count from resume where Id = ?");

            preparedStatement.setInt(1, Resume_id);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            check = resultSet.getInt("count") != 0;

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return check;
    }

    protected int SelectResume_id(int Student_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int Id = 0;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Select Id from resume where Student_id = ? order by Id desc");

            preparedStatement.setInt(1, Student_id);

            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            Id = resultSet.getInt("Id");

            preparedStatement.close();
            resultSet.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}

        return Id;
    }

}
