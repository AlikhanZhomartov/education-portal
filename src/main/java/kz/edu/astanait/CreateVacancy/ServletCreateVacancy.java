package kz.edu.astanait.CreateVacancy;

import kz.edu.astanait.GetSession.GetSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletCreateVacancy")
public class ServletCreateVacancy extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



        String job_title = request.getParameter("job_title").trim();
        String company_title = request.getParameter("company_title").trim();
        String jober_city = request.getParameter("jober_city").trim();
        int salary = Integer.parseInt(request.getParameter("salary").trim());
        String job_description = request.getParameter("job_description").trim();

        CreateVacancyDB db = new CreateVacancyDB();
        GetSession gs = new GetSession();

            db.CreateVacancy(job_title, company_title, jober_city, salary, job_description, gs.GetIdSession(request, response));


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
