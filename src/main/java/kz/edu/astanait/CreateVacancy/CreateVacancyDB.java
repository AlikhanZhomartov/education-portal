package kz.edu.astanait.CreateVacancy;

import kz.edu.astanait.DBConnection.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateVacancyDB extends DBConnection {

    protected void CreateVacancy(String Job_title, String Company_title, String Jober_city, int Job_salary, String Job_description, int Employee_id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement("Insert into requested_vacancy (Job_title, Company_title, Jober_city, Job_salary, Job_description, Employee_id) value(?, ?, ?, ?, ?, ?)");

            preparedStatement.setString(1, Job_title);
            preparedStatement.setString(2, Company_title);
            preparedStatement.setString(3, Jober_city);
            preparedStatement.setInt(4, Job_salary);
            preparedStatement.setString(5, Job_description);
            preparedStatement.setInt(6, Employee_id);

            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        }
        catch (SQLException e){e.printStackTrace();}
    }

}
