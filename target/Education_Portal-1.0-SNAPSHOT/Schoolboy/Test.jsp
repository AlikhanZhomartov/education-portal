<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.edu.astanait.Tests.Test" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/js/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/slick/slick-theme.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/test.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/slick/slick.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/test.js"></script>

</head>

<%ArrayList<Test> tests = (ArrayList<Test>) request.getAttribute("tests");%>

<body>
<section class="test">
    <div class="container">

        <div class="test__block__slider">
            <%for(int i = 0; i < tests.size(); i+=4){%>
            <div class="test__block">

                <p class="question">
                    <%=tests.get(i).getQuestion()%>
                </p>
                <div class="answer__options">

                    <div class="first__option option">
                        <input type="radio" class="first_input input_answer" name="input_answer_name<%=tests.get(i).getQuestion_id()%>" id="Radio<%=tests.get(i).getOption_id()%>" value="<%=tests.get(i).getTruth()%>">
                        <span class="answer__option">
                                <%=tests.get(i).getOption_text()%>
                        </span>
                    </div>
                    <div class="second__option option">
                        <input type="radio" class="first_input input_answer" name="input_answer_name<%=tests.get(i).getQuestion_id()%>" id="Radio<%=tests.get(i).getOption_id()%>" value="<%=tests.get(i + 1).getTruth()%>">
                        <span class="answer__option">
                                <%=tests.get(i + 1).getOption_text()%>
                            </span>
                    </div>
                    <div class="third__option option">
                        <input type="radio" class="first_input input_answer" name="input_answer_name<%=tests.get(i).getQuestion_id()%>" id="Radio<%=tests.get(i).getOption_id()%>" value="<%=tests.get(i + 2).getTruth()%>">
                        <span class="answer__option">
                                <%=tests.get(i + 2).getOption_text()%>
                        </span>
                    </div>
                    <div class="fourth__option option">
                        <input type="radio" class="first_input input_answer" name="input_answer_name<%=tests.get(i).getQuestion_id()%>" id="Radio<%=tests.get(i).getOption_id()%>" value="<%=tests.get(i + 3).getTruth()%>">
                        <span class="answer__option">
                                <%=tests.get(i + 3).getOption_text()%>
                            </span>
                    </div>

                </div>

                <div class="buttons__slider">

                    <button class="button_submit" id="Id<%=tests.get(i).getQuestion_id()%>">
                        ��������
                    </button>
                    <button class="button_delete" id="IdDel<%=tests.get(i).getQuestion_id()%>" style="display: none">
                        �������� �����
                    </button>

                    <form method="post" action="<%=request.getContextPath()%>/ServletSelectResult">
                    <button class="button_finish" type="submit">
                        ���������
                    </button>
                    </form>

                </div>
                <script>

                    $(document).ready(function (){
                        $("#Id<%=tests.get(i).getQuestion_id()%>").click(function () {

                            var result = $("input[name=input_answer_name<%=tests.get(i).getQuestion_id()%>]:checked").val();

                            $.ajax({
                                type: "POST",
                                url: "<%=request.getContextPath()%>/ServletCountResult",
                                data: {
                                    result
                                },
                                success: function(){
                                    $("#Id<%=tests.get(i).getQuestion_id()%>").css("display", "none");
                                    $("#IdDel<%=tests.get(i).getQuestion_id()%>").css("display", "block");
                                    $("input[name=input_answer_name<%=tests.get(i).getQuestion_id()%>]").attr('disabled', true);
                                    if(<%=i + 3%> != <%=tests.size() - 1%>) {
                                        $('.test__block__slider').slick('slickNext')
                                    }
                                }
                            });
                        })
                    });

                </script>
                <script>

                    $(document).ready(function (){
                        $("#IdDel<%=tests.get(i).getQuestion_id()%>").click(function () {

                            var resultDel = $("input[name=input_answer_name<%=tests.get(i).getQuestion_id()%>]:checked").val();

                            $.ajax({
                                type: "POST",
                                url: "<%=request.getContextPath()%>/ServletMinusCount",
                                data: {
                                    resultDel
                                },
                                success: function(){
                                    $("#IdDel<%=tests.get(i).getQuestion_id()%>").css("display", "none");
                                    $("#Id<%=tests.get(i).getQuestion_id()%>").css("display", "block");
                                    $("input[name=input_answer_name<%=tests.get(i).getQuestion_id()%>]").attr('disabled', false);
                                }
                            });
                        })
                    });

                </script>
            </div>
            <%}%>
        </div>
    </div>



</section>

</body>

</html>