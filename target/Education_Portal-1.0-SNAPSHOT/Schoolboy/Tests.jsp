<%@ page import="kz.edu.astanait.Tests.TestList" %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/ListOfTests.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
</head>
<%ArrayList<TestList> tests = (ArrayList<TestList>) request.getAttribute("test");%>
<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="picture">
            </a>
            <p class="logo__title">
                Job. S
            </p>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">

                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Schoolboy/Settings.jsp">���������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>

            </ul>

        </nav>

    </div>
</header>


<section class="test_list">

    <div class="container">

        <%for(int i = 0; i < tests.size(); i += 3){%>
        <div class="test__lists__three">

            <div class="test__block">

                <img class="block__img" src="<%=tests.get(i).getImage()%>" alt="picture">
                <p class="test__title">
                    <%=tests.get(i).getTitle()%>
                </p>
                <form method="post" action="<%=request.getContextPath()%>/ServletTestSelectByOne">
                <button class="test__btn" name="sbm_btn" value="<%=tests.get(i).getId()%>">
                  ������
                </button>
                </form>
            </div>
            <%if(tests.size() > i + 1){%>
            <div class="test__block">
                <img class="block__img" src="<%=tests.get(i + 1).getImage()%>" alt="picture">
                <p class="test__title">
                    <%=tests.get(i + 1).getTitle()%>
                </p>
                <form method="post" action="<%=request.getContextPath()%>/ServletTestSelectByOne">
                <button class="test__btn" name="sbm_btn" value="<%=tests.get(i + 1).getId()%>">
                 ������
                </button>
                </form>
            </div>
            <%}%>
            <%if(tests.size() > i + 2){%>
            <div class="test__block">
                <img class="block__img" src="<%=tests.get(i + 2).getImage()%>" alt="picture">
                <p class="test__title">
                    <%=tests.get(i + 2).getTitle()%>
                </p>
                <form method="post" action="<%=request.getContextPath()%>/ServletTestSelectByOne">
                <button class="test__btn" name="sbm_btn" value="<%=tests.get(i + 2).getId()%>">
                  ������
                </button>
                </form>
            </div>
            <%}%>

        </div>

        <%}%>
    </div>

</section>


</body>

</html>