<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/Resume.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>

</head>

<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>

            <form method="post" action="<%=request.getContextPath()%>/ServletListOfVacancy">
            <button class="header__btn">
                ��������
            </button>
            </form>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">

                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Student/Settings.jsp">���������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                    <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>

            </ul>

        </nav>


    </div>

</header>

<%int count = (int) request.getAttribute("count");%>

<section class="Resume__section">

    <div class="container">

        <%if(count == 0){%>
        <form method="post" action="<%=request.getContextPath()%>/ServletCreatResume">
                <%}%>
            <%if (count != 0){%>
        <form method="post" action="<%=request.getContextPath()%>/ServletUpdateResume">
       <%}%>
        <div class="Resume__section__inner">

            <h2 class="Resume__title">
                ���� ������
            </h2>

            <div class="Resume__name resume__block">

                <p class="name__title resume_inner_title">
                    ���
                </p>

                <input type="text" class="Resume__name__input resume__input" placeholder="���" <c:forEach var="resume" items="${resume}"> value="${resume.student_name}" </c:forEach> name="name" autocomplete="off" required>

            </div>

            <div class="Resume__surname resume__block">

                <p class="surname__title resume_inner_title">
                    �������
                </p>

                <input type="text" class="Resume__surname__input resume__input" placeholder="�������" <c:forEach var="resume" items="${resume}"> value="${resume.student_surname}" </c:forEach> name="surname" autocomplete="off" required>

            </div>

            <div class="Resume__mobile__number resume__block">

                <p class="mobile__number__title resume_inner_title">
                    ��������� �������
                </p>

                <input type="text" class="mobile__number__input resume__input"  placeholder="+7" <c:forEach var="resume" items="${resume}"> value="${resume.student_mobile}" </c:forEach> name="mobile" autocomplete="off" required>

            </div>

            <div class="Resume__city resume__block">

                <p class="city__title resume_inner_title">
                    ����� ����������
                </p>

                <input type="text" class="city__input resume__input" <c:forEach var="resume" items="${resume}"> value="${resume.student_city}" </c:forEach> name="city" autocomplete="off" required>

            </div>
            <%if(count == 0){%>
            <button class="resume_btn" name="resume_btn">���������</button>
                    <%}%>
                    <%if (count != 0){%>
            <button class="resume_btn" name="resume_btn">��������</button>
                        <%}%>
        </div>
        </form>

    </div>

    </div>

    </div>

</section>
</body>

</html>