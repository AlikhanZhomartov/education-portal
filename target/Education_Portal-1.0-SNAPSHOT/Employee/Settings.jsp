<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/ChangePassword.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</head>

<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>

            <form method="post" action="<%=request.getContextPath()%>/ServletListOfVacancyForEmployee">
                <button class="header__btn">
                    ��������
                </button>
            </form>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">

                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletSelectResponses">
                        <button class="menu__list-link__btn">
                            �������
                        </button>
                    </form>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletEmployeeVacancy">
                        <button class="menu__list-link__btn">��� ��������</button>
                    </form>
                </li>
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Employee/CreateVacancy.jsp">������� ��������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>

            </ul>

        </nav>


    </div>

</header>



<section class="change__password__section">

    <div class="container">


        <div class="change__password__inner">


            <h2 class="change__password__title">
                ������� ������
            </h2>
                <div class="change__password__email change__password">

                    <p class="email__password change__password__inner__title">
                        Email
                    </p>

                    <input type="text" class="change__password__email__input change__password__input" placeholder="Email" name="Email" id="email" autocomplete="off">

                </div>

                <div class="change__password__old change__password">

                    <p class="old__password change__password__inner__title">
                        ������ ������
                    </p>

                    <input type="text" class="change__password__old__input change__password__input" placeholder="������ ������" name="OldPassword" id="OldPassword" autocomplete="off">

                </div>

                <div class="change__password__new change__password">

                    <p class="new__password change__password__inner__title">
                        ����� ������
                    </p>

                    <input type="text" class="change__password__new__input change__password__input" placeholder="����� ������" name="NewPassword" id="NewPassword" autocomplete="off">

                </div>

                <div class="change__password__repeated change__password">

                    <p class="repeated__password change__password__inner__title">
                        ��������� ����� ������
                    </p>

                    <input type="text" class="change__password__repeated__input change__password__input"  placeholder="��������� ����� ������" name="RepeatedPassword" id="RepeatedPassword" autocomplete="off">

                </div>


                <button class="change__password__btn" onclick="changePassword()">���������</button>

        </div>
    </div>

    </div>
</section>
</body>
<script>
    function changePassword() {
        if($("#email").val() != "" && $("#OldPassword").val() != "" && $("#NewPassword").val() != "" && $("#RepeatedPassword").val() != "") {
            $.ajax({
                type: "POST",
                url: "<%=request.getContextPath()%>/ServletChangePassword",
                async: true,
                data: {
                    "Email": $("#email").val(),
                    "OldPassword": $("#OldPassword").val(),
                    "NewPassword": $("#NewPassword").val(),
                    "RepeatedPassword": $("#RepeatedPassword").val(),
                },
                success: function (result) {
                    $("#email").val("");
                    $("#OldPassword").val("");
                    $("#NewPassword").val("");
                    $("#RepeatedPassword").val("");

                }
            });
        }
    }
</script>
</html>