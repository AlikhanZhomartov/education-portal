<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.edu.astanait.EmployeeVacancy.EmployeeVacancy" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/VacancyList.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
</head>

<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>

            <form method="post" action="<%=request.getContextPath()%>/ServletListOfVacancyForEmployee">
            <button class="header__btn">
                ��������
            </button>
            </form>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Employee/CreateVacancy.jsp">������� ��������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletSelectResponses">
                        <button class="menu__list-link__btn">
                            �������
                        </button>
                    </form>
                </li>
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Employee/Settings.jsp">���������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>

            </ul>

        </nav>


    </div>



</header>


<section class="job__vacancy">

    <div class="container">

        <c:forEach items="${EmployeesVacancy}" var="vacancy">
        <div class="job__vacancy__inner">

            <form method="post" action="<%=request.getContextPath()%>/ServletVacancyForEmployee">
            <div class="job__vacancy__inner__block">
                <div class="job__vacancy__title_salary">
                    <h3 class="job__vacancy_title">
                    ${vacancy.job_title}
                    </h3>
                    <span class="job__vacancy__salary">
                            ${vacancy.job_salary}
                        </span>
                </div>

                <p class="job__vacancy__company">
                        ${vacancy.company_title}
                </p>
                <p class="job__vacancy__city">
                        ${vacancy.jober_city}
                </p>
                <p class="job__vacancy__description">
                        ${vacancy.job_description}
                </p>
                <button type="submit" class="job__vacancy__response" name="Id" value="${vacancy.id}">
                    �������
                </button>

            </div>
        </form>
        </div>
        </c:forEach>

    </div>
</section>


</body>

</html>