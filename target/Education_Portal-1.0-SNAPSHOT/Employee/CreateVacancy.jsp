<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/CreateVacancy.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</head>

<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>

            <form method="post" action="<%=request.getContextPath()%>/ServletListOfVacancyForEmployee">
            <button class="header__btn">
                ��������
            </button>
            </form>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletSelectResponses">
                        <button class="menu__list-link__btn">
                            �������
                        </button>
                    </form>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletEmployeeVacancy">
                    <button class="menu__list-link__btn">��� ��������</button>
                    </form>
                </li>

                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Employee/Settings.jsp">���������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>

            </ul>

        </nav>


    </div>

</header>



<section class="Creat__vacancy__section">

    <div class="container">

        <div class="Creat__vacancy__section__inner">


            <h2 class="Creat__vacancy__blocks__title">
                ���������� � ��������
            </h2>

            <div class="Creat__vacancy__title__input Creat__vacancy__block">

                <p class="Creat__vacancy__title Creat__vacancy_inner_title">
                    �������� ������
                </p>

                <input type="text" class="Creat__vacancy__title__input Creat__vacancy__input" name="job_title" id="job_title" placeholder="������� �������� ������" autocomplete="off">

            </div>

            <div class="Creat__vacancy__company__title__input Creat__vacancy__block">

                <p class="Creat__vacancy__company__title Creat__vacancy_inner_title">
                    �������� ��������
                </p>

                <input type="text" class="Creat__vacancy__company__input Creat__vacancy__input" name="company_title" id="company_title"
                       placeholder="������� �������� ��������" autocomplete="off">

            </div>

            <div class="Creat__vacancy__city Creat__vacancy__block">

                <p class="Creat__vacancy__city__title Creat__vacancy_inner_title">
                    ����� ���������
                </p>

                <input type="text" class="Creat__vacancy__city__input Creat__vacancy__input" name="jober_city" id="jober_city"
                       placeholder="������� �������� ������ ���������" autocomplete="off">

            </div>

            <div class="Creat__vacancy__salary Creat__vacancy__block">

                <p class="Creat__vacancy__salary__title Creat__vacancy_inner_title">
                    ������ �����
                </p>

                <input type="number" class="Creat__vacancy__salary__input Creat__vacancy__input" name="salary" id="salary"
                       placeholder="������� ����� ������ �����" autocomplete="off">

            </div>

            <div class="Creat__vacancy__information Creat__vacancy__block">

                <p class="Creat__vacancy__information__title Creat__vacancy_inner_title">
                    �������� ������
                </p>

                <textarea  class="Creat__vacancy__information__input Creat__vacancy__input" name="job_description" id="job_description" required></textarea>

            </div>
            <div class="Creat__vacancy__btn Creat__vacancy__block">
                <button class="Creat__vacancy_btn" onclick="createBtnPressed()">���������</button>
            </div>
        </div>
    </div>


    </div>




</section>


</body>
<script>
        function createBtnPressed() {
            if($("#job_title").val() != "" && $("#company_title").val() != "" && $("#jober_city").val() != "" && $("#salary").val() != "" && $("#job_description").val() != "") {
            $.ajax({
                type: "POST",
                url: "<%=request.getContextPath()%>/ServletCreateVacancy",
                async: true,
                data: {
                    "job_title": $("#job_title").val(),
                    "company_title": $("#company_title").val(),
                    "jober_city": $("#jober_city").val(),
                    "salary": $("#salary").val(),
                    "job_description": $("#job_description").val(),
                },
                success: function (result) {
                    $("#job_title").val("");
                    $("#company_title").val("");
                    $("#jober_city").val("");
                    $("#salary").val("");
                    $("#job_description").val("");

                }
            });
        }
    }
</script>
</html>