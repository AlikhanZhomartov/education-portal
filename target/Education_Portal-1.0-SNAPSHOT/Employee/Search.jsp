<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/Student.css">
    <%@ page language="java" contentType="text/html;charset=cp1251"%>
</head>

<body>


<header class="header">

    <div class="header__inner">

        <div class="logo__btn">
            <a class="logo__link">
                <img class="logo__img" src="<%=request.getContextPath()%>/resources/images/logo1.jpg" alt="logo">
            </a>

            <form method="post" action="<%=request.getContextPath()%>/ServletSelectResponses">
                <button class="header__btn">
                    �������
                </button>
            </form>
        </div>
        <nav class="header__menu">
            <ul class="menu__list">
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletListOfVacancyForEmployee">
                        <button class="menu__list-link__btn">��������</button>
                    </form>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletEmployeeVacancy">
                        <button class="menu__list-link__btn">��� ��������</button>
                    </form>
                </li>
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Employee/CreateVacancy.jsp">������� ��������</a>
                </li>
                <li class="menu__list-item">
                    <a class="menu__list-link" href="<%=request.getContextPath()%>/Employee/Settings.jsp">���������</a>
                </li>
                <li class="menu__list-item">
                    <form method="post" action="<%=request.getContextPath()%>/ServletLogOut">
                        <button class="menu__list-link__btn">�����</button>
                    </form>
                </li>
            </ul>

        </nav>


    </div>



</header>

<section class="searcher_section">
    <div class="searcher">

        <div class="container">
            <form method="post" action="<%=request.getContextPath()%>/ServletSearchForEmployee">
            <div class="searcher__inner">
                <div class="searcher__input">

                    <input type="text" class="searching__text" name="content" placeholder="���������, �������� � �����" autocomplete="off">

                </div>
                <div class="searching__btn">
                    <button type="submit" class="searching__btn-sub">�����</button>
                </div>
            </div>
            </form>
        </div>

    </div>
</section>

<section class="job__vacancy">

    <div class="container">
        <div class="job__vacancy__inner">

            <c:forEach var="vacancy" items="${result}">
                <div class="job__vacancy__inner__block">
                    <div class="job__vacancy__title_salary">
                        <h3 class="job__vacancy_title">
                                ${vacancy.job_title}
                        </h3>
                        <span class="job__vacancy__salary">
                            ${vacancy.job_salary} KZT
                        </span>
                    </div>

                    <p class="job__vacancy__company">
                            ${vacancy.company_title}
                    </p>
                    <p class="job__vacancy__city">
                            ${vacancy.jober_city}
                    </p>
                    <p class="job__vacancy__description">
                            ${vacancy.job_description}
                    </p>
                </div>
            </c:forEach>




        </div>
    </div>


</section>
</body>

</html>